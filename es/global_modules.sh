#!/usr/bin/env bash
set -e

# node version manager
# https://github.com/tj/n
yarn global add n

# latest, stable node release
n stable

# testing framework
yarn global add ava tern jsonlint prettier remark speed-test
