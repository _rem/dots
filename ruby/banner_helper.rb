# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

# Message, and color palettes must have same number of lines to avoid
# glitches.
def banner message, palette
  message.zip(palette).each { |line| puts dye(*line) }
end

def dye line, color
  "\e[38;5;#{color};01m#{line}\e[0m"
end
