# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

# Usage:
# at_exit { require_relative "./ah.rb" }

require_relative "./banner_helper.rb"

# Message spans 6 lines. Color palettes must have as many entries.
AH = [
  "     _=_",
  "    (   )",
  "    _) (_",
  "   /__/  \\",
  " _(<_   / )_",
  "(__\\_\\_|_/__)"
]


BLUE = %w[
  021
  027
  033
  032
  039
  038
]

DEVELOPMENT = %w[
  226
  190
  154
  118
  046
  047
]

STAGING = %w[
  208
  214
  220
  226
  190
  154
]

PRODUCTION = %w[
  160
  196
  202
  208
  214
  220
]

banner AH, BLUE
