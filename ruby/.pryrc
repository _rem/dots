# -*- ruby -*-
Pry.config.theme = "ocean"
Pry.config.editor = proc { |file, line| "emacsclient +#{line} #{file}" }
# Pry.config.auto_indent = false # Comment out if repling outside emacs

aliases = {
  "lM" => "ls -M",
  "lm" => "ls -m",
  "fm" => "find-method",
  "si" => "show-input",
  "al" => "amend-line",
  "st" => "stat",
  "wai" => "whereami",
  "tmp" => "edit -t"
}

aliases.each_pair { |nick, cmd| Pry.config.commands.alias_command nick, cmd }
