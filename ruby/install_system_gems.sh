#!/usr/bin/env sh

echo "Removing bundled gems"
gem uninstall net-telnet power_assert test-unit xmlrpc

echo; echo; echo "Installing toolbox"
gem install minitest roda sequel

echo; echo; echo "Installing minitest plugins"
gem install minitest-proveit minitest-autotest --no-document

echo; echo; echo "Installing code smell reporters"
gem install debride flog flay ruby2ruby --no-document

# echo; echo; echo "Installing REPL tools..."
# gem install pry-byebug pry-theme pry-doc --no-document

echo; echo; echo "Installing gem management tools"
gem install bundler bundler-audit --no-document
# hanna-nouveau # NOTE @ gemrc; rdoc: --format hanna

echo; echo; echo "Installing Spacemacs linters"
gem install seeing_is_believing reek slim scss_lint\
    scss_lint_reporter_checkstyle --no-document

echo; echo; echo "Run update/system_gems get the latest default gems"
