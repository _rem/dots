# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

# SEE YOU SPACE COWBOY by René Maya
# based on the one by DANIEL REHN (danielrehn.com)
# as found at: https://rubyexample.com/snippet/seeyouspacecowboyrb_dwm348_ruby

# Usage:
# at_exit { require_relative "./bye.rb" }

require_relative "./banner_helper.rb"

# Message spans 21 lines. Color palettes must have as many entries.
BEBOP = [
    '  .d8888b. 88888888 88888888   Yb     dP .d888b.  88     88',
    '  88.    8 88       88          Yb   dP d8P" "Y8b 88     88',
    '   Y8b.    88       88           Yb.P   88     88 88     88',
    '     "Yb.  888888   888888        88    88     88 88     88',
    '       "8. 88       88            88    88     88 88     88',
    '  8.   .88 88       88            88    Y8b. .d8P Y8b. .d8P',
    '  "Y8888P  88888888 88888888      88     "Y888P"   "Y888P" ',
    '  .d8888b. 888888b.    d888  .d8888b. 88888888             ',
    '  88.    8 88   Y8b   d8888 d8P   Y8b 88                   ',
    '   Y8b.    88   d8P  d8P 88 88        88                   ',
    '     "Yb.  888888"  d8P  88 88        888888               ',
    '       "8. 88      d8P   88 88     88 88                   ',
    '  8.   .88 88     d88888888 Y8b   d8P 88                   ',
    '  "Y8888P  88    d8P     88  "Y888P"  88888888             ',
    '  .d8888b.  .d888b.  88       88 88888b.   .d888b. Yb    dP',
    ' d8P   Y8b d8P" "Y8b 88       88 88  "8P  d8P" "Y8b Yb  dP ',
    ' 88        88     88 88   .   88 88  dK   88     88  Yb.P  ',
    ' 88        88     88 88  d8b  88 8888"Yb  88     88   88   ',
    ' 88     88 88     88 88.P   Y.88 88    88 88     88   88   ',
    ' Y8b   d8P Y8b. .d8P 88P     Y88 88   d8P Y8b. .d8P   88   ',
    '  "Y888P"   "Y888P"  8P       Y8 88888P"   "Y888P"    88   '
]

PRIDE = %w[
  160
  196
  202
  208
  214
  220
  226
  190
  154
  118
  046
  047
  048
  049
  051
  039
  027
  021
  021
  057
  093
]

BLUE = %w[
  019
  020
  021
  027
  033
  032
  039
  038
  045
  044
  051
  044
  045
  038
  039
  032
  033
  027
  021
  020
  019
]

banner BEBOP,BLUE
