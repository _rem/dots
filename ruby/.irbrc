# -*- ruby -*-
# -*- coding: utf-8 -*-

require_relative "./repl"

include REPL

IRB.conf[:LOAD_MODULES] = REPL.modules
IRB.conf[:USE_READLINE] = REPL.readline
IRB.conf[:HISTORY_FILE] = REPL.history
IRB.conf[:SAVE_HISTORY] = REPL.history_size
IRB.conf[:IRB_NAME] = REPL.session_name
IRB.conf[:AUTO_INDENT] = REPL.auto_indent
IRB.conf[:PROMPT][:CONTEXTUAL] = REPL.contextual_prompts
IRB.conf[:PROMPT_MODE] = :CONTEXTUAL # :NULL, :DEFAULT, :CLASSIC, :SIMPLE, :INF_RUBY

auto_load_once = {
  aliases: -> s { _aka }
  # DB.loggers << Logger.new($stdout) if defined?(DB) && DB.respond_to?(:loggers)
}

IRB.conf[:IRB_RC] = -> session {
  session.inspect_mode = :pp

  # stuff auto-loaded once 'cause apparently it must to be done manually thereafter
  auto_load_once.each { |k, l| l.call session; auto_load_once.delete k }
}

IRB.conf[:AT_EXIT] << -> { require_relative "./ah.rb" }
