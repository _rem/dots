#!/usr/bin/env sh

sudo -H pip install --upgrade pip
sudo apt update && sudo apt dist-upgrade -y
# sudo apt update && sudo apt full-upgrade -y

# Ruby build dependencies
sudo apt update
sudo apt upgrade -y gcc-6 autoconf bison build-essential libssl-dev libyaml-dev \
     libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev

# System tools
sudo apt update
sudo apt upgrade -y git software-properties-common httpie zsh htop shellcheck\
     tree sed xdg-utils gufw deluge chkrootkit ppa-purge python-pip keychain\
     blueman silversearcher-ag

# Development tools
sudo apt update
sudo apt remove cmdtest && sudo apt update # cli testing lib. Conflicts w/ yarn.js
sudo apt upgrade -y emacs25 heroku nodejs yarn libpq-dev libsodium-dev

# Remove upgrade temp dependencies
sudo apt-get autoclean && sudo apt autoremove -y

# Ruby build https://github.com/rbenv/ruby-build/wiki#updating-ruby-build
# cd $(which ruby-build) && git pull # ??
