#!/usr/bin/env sh

echo "RubyGems"
gem update --system --no-document

echo; echo; echo "Utilities"
gem update minitest-proveit minitest-autotest debride flog flay\
    ruby2ruby bundler bundler-audit did_you_mean fiddle irb\
    seeing_is_believing reek sass slim scss-lint\
    scss_lint_reporter_checkstyle --no-document

echo; echo; echo "Gemified libraries"
gem update benchmark bigdecimal cgi csv date dbm delegate etc fcntl\
    fileutils forwardable gdbm io-console ipaddr json logger matrix\
    mutex_m observer open3 openssl ostruct prime psych rdoc readline\
    readline-ext reline rexml rss sdbm stringio strscan timeout tracer\
    uri webrick yaml zlib --document ri

echo; echo; echo "Toolbox"
gem update minitest rake roda sequel --document ri

gem cleanup
