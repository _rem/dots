## Install

Bodhi Linux prerequisites.

```terminal
$ sudo apt update && sudo apt install -y git
```

After prerequisites clone repo.

```terminal
$ git clone https://notabug.org/rem/.dots.git
```

Edit `install/macOS_prefs.sh` or `install/bodhi_prefs.sh`.
Add sensitive dots to `private` folder. Samples at `/private_sample`
Specify PostgreSQL version in `install/postgres.sh` for Bodhi.
Run the installation script

```terminal
$ . .dots/install.sh
```

## Manual Installation

### Bodhi Linux

#### Ruby

```terminal
$ ruby-build --definitions  # lists available Rubies
$ ry install <version>
$ . ~/.dots/ruby/install_system_gems.sh
```

#### PostgreSQL

```terminal
$ su -c "psql" - postgres
psql> \password
psql> \q
$ sudo service postgresql restart
```

#### Racket

Download from [website](https://download.racket-lang.org)
and install with

```terminal
$ sudo sh racket-VERSION-x86_64-linux.sh
```

Use default setting during installation. When asked about
new system links set path to `/usr` so binstubs are added
to Path.

If desktop symlinks don't work, as in v6.9, run Dr. Racket
from terminal.

```terminal
$ drracket
```

#### Remove 3rd Party Repos' Packages

Completely uninstall apps downloaded from 3rd party repos:

```terminal
$ sudo apt autoremove <package>
$ sudo ppa-purge ppa:<repo/path>
```

#### Linux: VirtualBox's guest

In order to integrate a guest linux system to its host
we need to install Guest Additions.

First, install a few dependencies.

```terminal
$ sudo apt update && sudo apt install -y dkms # build-essentials linux-headers-generic (not for Bodhi)
```

The recommended way to install GA is to download the
matching version to the host's VirtualBox and insert
the image into the VM. If ISO image doesn't autorun,

```terminal
$ sudo mount /dev/cdrom /media/cdrom && cd /media/cdrom && sudo sh -y VBoxLinuxAdditions.run
$ reboot
```

If that doesn't work get the latest version on the apt repo.

```terminal
$ sudo apt update && sudo apt install -y virtualbox-guest-additions-iso
```

Update the host's VirtualBox, and repeat the steps above
although you might need to mount from `/usr/share/virtualbox/VBoxGuestAdditions.iso`

Sources:

- [How do I install Guest Additions in a VirtualBox VM?](https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm)
- [HOWTO: Install Linux Guest Additions + Xorg config](https://forums.virtualbox.org/viewtopic.php?f=3&t=15679)

##### Remove Guest Additions

If Guest Additions aren't installed properly they might not
prevent booting into the GUI. Hold `Shift` during startup
to get the GRUB menu. Access a root shell then:

```terminal
$ cd /opt/<VirtualBoxAdditions-x.x.xx>/
$ sudo sh ./uninstall.sh
$ reboot
```

Try reinstalling after rebooting.


### MacOS
Verify rbenv is in the PATH `$ env | grep PATH`. If not
there start a new terminal session before heading to
rbenv's docs.

#### Ruby

```terminal
$ rbenv install -l
$ rbenv install <version>
$ . ~/.dots/ruby/install_system_gems.sh
```

#### PostgreSQL

```terminal
$ psql -U postgres
psql> \password
psql> \q
$ brew services restart postgresql
```

## Settings & Prefs

### Common Setup

#### Pry-theme

```terminal
$ pry
> pry-theme install ocean
```

#### Heroku CLI

Login to Heroku via `$ heroku login`

Opt-out from analytics. Open `~/.heroku/config.json` and set `skip_analytics: true`.

#### Firefox

Check `firefox/ReadMe.org` for details on configuring Firefox.


### Bodhi Linux settings

#### Wallpapers

Add desktop wallpaper to `~/.e/e/backgrounds`

#### Shelves

Go `Settings > Shelves`

For `shelf > Contents` remove gadgets:

- `iBar`
- `System`
- `Tasks`

Furthermore, under `Settings`:
- `Stacking > Above Everything`
- `Position > (top)`
- `Size > Height (24 pixels)`
- `Auto Hide > auto-hide the self`
- `Hide timeout > 0.2 seconds`
- `Hide duration > 0.35 seconds`

Add `dock` shelf. Under `Contents` add gadgets:

- `iBar`

Under `Settings`:

- `Stacking > Above Everything`
- `Position > (bottom)`
- `Size > Height (48 pixels)`
- `Auto Hide > auto-hide the self`
- `Hide timeout > 0.2 seconds`
- `Hide duration > 0.35 seconds`

#### Fonts

- [Source Code Pro](https://fonts.google.com/specimen/Source+Code+Pro?selection.family=Source+Code+Pro). Spacemacs active font.
- [Input](http://input.fontbureau.com/)
- [Anonymous Pro](https://www.marksimonson.com/fonts/view/anonymous-pro)


#### System preferences

- MokshaArcDark (desktop)
- nyanology (terminal)
- Arc Dark Blue (albert)

#### UFW
Add rules via Terminology ie.

```terminal
$ ufw default deny incoming && ufw default deny outgoing
$ ufw allow out 21,22,53,80,443/tcp
$ ufw allow out 53,67,68/udp

# IRC
$ ufw allow out 6667:7000

# Transmission
$ ufw allow out 51413/tcp
$ ufw allow out 51413/udp
$ ufw allow out 6969/tcp
```

Restart UFW to apply changes.

```terminal
$ ufw disable && ufw enable
```

Some common ports are:

FTP - 21 TCP, SSH - 22 TCP, TELNET - 23 TCP, SMTP - 25 TCP<br>
DNS - 53 TCP/UDP * mandatory<br>
DHCP - 67, 68 DHCP<br>
HTTP - 80 TCP<br>
POP3 - 110 TCP, IMAP - 143 TCP<br>
HTTPS - 443 TCP<br>
VNC - 5900-6000<br>
Gmail SMTP-TLS: 587, SMTP-SSL: 465, POP-SSL: 995, IMAP-SSL: 993

Further details:

- [List of TCP/UDP ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)
- [How To Setup a Firewall with UFW](https://kaosx.us/docs/ufw/)
- [How to setup UFW](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-14-04)
- [Ubuntu Basic Security: Firewall](https://wiki.ubuntu.com/BasicSecurity/Firewall)
- [UFW common rules and commands](https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands)
- [Intro to UFW](https://help.ubuntu.com/community/UFW)
- [UFW docs](https://wiki.ubuntu.com/UncomplicatedFirewall)
- [Intro to Gufw](https://help.ubuntu.com/community/Gufw)
- [Gufw docs](http://gufw.org/)

### MacOS settings

#### Set Zsh as default shell

Set system up so it uses brewed zsh by default. Confirm brew
zsh location:

```terminal
$ brew list zsh
```
Open shells; it sets which shells the system knows about.

```terminal
$ sudo nano /etc/shells
```

At the end of the files add:

```text
/bin/zsh               #if its not already there
/usr/local/bin/zsh
```

Set brewed zsh as the system's default shell

```terminal
$ chsh -s /usr/local/bin/zsh
```

#### Symlink Emacs to Applications folder
Get brew emacs' version and symlink the correct version.

```terminal
$ brew list emacs
$ ln -sfv "$(brew --prefix)/Cellar/emacs/VERSION/Emacs.app" /Applications
```

#### System preferences
- Reduce transparency. `System Preferences > Accessibility > Display > Reduce transparency`
- Reduce motion. `System Preferences > Accessibility > Display > Reduce motion`
- Three finger drag. `System Preferences > Accessibility > Mouse & Trackpad > Trackpad Options`, mark `Enable dragging` and finally from the pop up menu choose `three finger drag`
- Remap CAPS to Alt/option/Meta and viceversa. `System Preferences > Keyboard >Modifier keys`
- Cancel use Apple remote. `System Preferences > Security & Privacy > General > Advanced` and mark `Disable remote control infrared receiver`
- Change finder sidebar. On a finder window: `Finder > preferences > sidebar`
- Remove items from the Thrash after 30 days: `Finder > preferences > advanced`, tick option thus named.
- iTerm2 remap right option to ESC `Preferences > Profiles tab > Keys sub-tab > window bottom`
- Turn off notifications `System Preferences > Notifications > Do Not Disturb`, set clock from 05:41 to 05:40.

## System Updates

It's always better to update each app and library one at time. If you just want
to update everything at once run, from `update.sh`'s folder,

```terminal
$ . update.sh
```
