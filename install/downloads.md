# Apps for manual installation

## Mac Apps
[alfred](https://www.alfredapp.com/)
[iterm2](https://iterm2.com/downloads.html)
[firefox](https://www.mozilla.org/en-US/firefox/new/)
[transmission](https://github.com/transmission/transmission-releases/)
[app_zapper](https://appzapper.com/)
[plug](https://www.plugformac.com/updates/plug2/Plug-latest.dmg)
[vlc](https://www.videolan.org/vlc/download-macosx.html)
[onyx](https://www.titanium-software.fr/en/onyx.html)
[deeper](https://www.titanium-software.fr/en/deeper.html)
[scene builder](https://gluonhq.com/download/scene-builder-mac/)
[pencil](https://pencil.evolus.vn/)
[wire](https://wire.com/en/download/)
[ReiKey](https://objective-see.com/products/reikey.html)
[DoNotDisturb](https://objective-see.com/products/dnd.html)
[LuLu](https://objective-see.com/products/lulu.html)

### Mac App Store:
memory_cleaner
dr. cleaner

## Linux Apps
[zazu](https://github.com/tinytacoteam/zazu/releases/download/v0.4.0/zazu_0.4.0_amd64.deb)
[slack](https://slack.com/downloads/linux)
[firefox](https://www.mozilla.org/en-US/firefox/new/)
[dropbox](https://www.dropbox.com/install-linux)
[telegram](https://desktop.telegram.org/)
[scene builder](https://gluonhq.com/download/scene-builder-linux-deb/)
[pencil](https://pencil.evolus.vn/)

Sql clients:
[sqlitebrowser](https://sqlitebrowser.org/)
[DBeaver](https://dbeaver.jkiss.org/)
[pgadmin](https://www.pgadmin.org/download/)
