#!/usr/bin/env sh
set -e

ln -sfv ~/.dots/git/.gitconfig ~/.gitconfig
ln -sfv ~/.dots/git/.gitignore_global ~/.gitignore_global
ln -sfv ~/.dots/git/.git_completion ~/.git_completion
ln -sfv ~/.dots/git/.githelpers ~/.githelpers
ln -sfv ~/.dots/git/templates/HEAD ~/.git_templates/HEAD
ln -sfv ~/.dots/ruby/.gemrc ~/.gemrc
ln -sfv ~/.dots/ruby/.bundle_config ~/.bundle/config
ln -sfv ~/.dots/ruby/.pryrc ~/.pryrc
ln -sfv ~/.dots/ruby/.irbrc ~/.irbrc
ln -sfv ~/.dots/ruby/.config.reek ~/.config.reek
mkdir -p ~/.pry/themes
ln -sfv ~/.dots/ruby/ocean.prytheme.rb ~/.pry/themes/ocean.prytheme.rb
ln -sfv ~/.dots/runcom/.nanorc ~/.nanorc
ln -sfv ~/.dots/runcom/.cmusrc ~/.cmusrc
ln -sfv ~/.dots/runcom/.sqliterc ~/.sqliterc

pdots="~/.dots/private"
[ -f "$pdots/.gitpersonal" ] && (ln -sfv "$pdots/.gitpersonal"  ~/.gitpersonal)
[ -f "$pdots/.ssh_config" ] && (ln -sfv "$pdots/.ssh_config" ~/.ssh/config)
[ -f "$pdots/.gem_credentials" ] && (ln -sfv "$pdots/.gem_credentials" ~/.gem/credentials)

if [[ "$OSTYPE" == "darwin"* ]]; then
  ln -sfv "$(brew --prefix)/share/git-core/contrib/diff-highlight/diff-highlight" /usr/local/bin/diff-highlight
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
  ln -sfv ~/.dots/ruby/ry_completion.zsh /usr/local/share/zsh/site-functions/_ry
fi
