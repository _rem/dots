#!/usr/bin/env sh
set -e

# Add computer name (@ local)
hostnamectl set-hostname ""

ln -sfv ~/.dots/bodhi/bar_app_order ~/.e/e/applications/bar/default/.order
ln -sfv ~/.dots/bodhi/startup_commands ~/.e/e/applications/startup/startupcommands
ln -sfv ~/.dots/bodhi/startup_app_order ~/.e/e/applications/startup/.order
cp -a /usr/share/enlightenment/data/themes/MokshaArcDark.edj ~/.e/e/themes/
