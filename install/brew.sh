# Install Homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
brew upgrade

# Install brew packages
brew install aspell --lang=en
brew install emacs --with-cocoa
brew install findutils
brew install ffmpeg
brew install git
brew install gnu-sed --with-default-names
brew install heroku
brew install httpie
brew install node
brew install yarn
brew install rbenv
brew install rbenv-vars
brew install rbenv-gem-rehash
brew install shellcheck
brew install ssh-copy-id
brew install tree
brew install zsh
brew install chkrootkit
brew install libsodium
brew install pass
brew install cmus
brew install source-highlight

# brew cask install keepassxc
