#!/usr/bin/env sh
set -e

# Prerequisites
sudo apt update && sudo apt install -y software-properties-common httpie
sudo apt update # refresh system utils

# Repos
sudo add-apt-repository -y "deb https://cli-assets.heroku.com/branches/stable/apt ./"
sudo add-apt-repository -y "deb https://dl.yarnpkg.com/debian/ stable main"
sudo add-apt-repository -y "deb https://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
sudo add-apt-repository -y ppa:alexmurray/emacs
sudo add-apt-repository -y ppa:zeal-developers/ppa # docs viewer
sudo add-apt-repository -y ppa:deluge-team/ppa
sudo add-apt-repository -y ppa:ubuntuhandbook1/apps # parole
# sudo add-apt-repository -y ppa:jonathonf/vlc
sudo add-apt-repository -y ppa:ubuntu-mozilla-daily/ppa
# sudo add-apt-repository -y ppa:nilarimogard/webupd8 # albert
sudo add-apt-repository -y ppa:nextcloud-devs/client

# Resources
http -d https://cli-assets.heroku.com/apt/release.key | sudo apt-key add -
http -d https://deb.nodesource.com/setup_7.x | sudo -E bash -
http -d https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
http -d https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

#(delete if no problem installing yarn!)
# echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# Refresh system and app lists
sudo apt update

# Ruby build dependencies
sudo apt install -y gcc-6 autoconf bison build-essential libssl-dev libyaml-dev \
     libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev

# System tools
sudo apt install -y zsh htop shellcheck tree sed xdg-utils gufw deluge chkrootkit \
     ppa-purge python-pip keychain blueman silversearcher-ag

# Development tools
sudo apt remove cmdtest && sudo apt update # cli testing lib. Conflicts w/ yarn.js
sudo apt install -y emacs25 heroku nodejs yarn libpq-dev libsodium-dev
# https://superuser.com/questions/71588/how-to-syntax-highlight-via-less
# libsource-highlight-common source-highlight

# diff-highlight
sudo -H pip install --upgrade pip
sudo -H pip install diff-highlight


# Ry (ruby manager) https://github.com/jneen/ry
cd ~ && git clone git://github.com/jneen/ry
cd ry
PREFIX=$HOME/.local make install

# Ruby-build https://github.com/rbenv/ruby-build
cd ~ && git clone https://github.com/rbenv/ruby-build.git
PREFIX=/usr/local ./ruby-build/install.sh
cd ~ # back home after installing rbenv

# can delete ~/ry && ~/ruby-build ??

# Install libsodium (delete if no problems installing libsodium-dev)
# https://download.libsodium.org/doc/installation/
# http -d https://download.libsodium.org/libsodium/releases/LATEST.tar.gz
# ./configure
# make && make check
# sudo make install

# Rbenv (delete if successfully installed Ry)
# git clone https://github.com/rbenv/rbenv.git ~/.rbenv
# cd ~/.rbenv && src/configure && make -C src
# export PATH="$HOME/.rbenv/bin:$PATH"
# mkdir -p "$(rbenv root)/plugins"
# cd "$(rbenv root)/plugins"

# declare -a plugins=(rbenv/ruby-build rkh/rbenv-update sstephenson/rbenv-gem-rehash rbenv/rbenv-default-gems)

# for plugin in ${plugins[@]}; do
#   git clone https://github.com/$plugin.git
# done

# Apps
sudo apt install -y pass evince zeal firefox-trunk parole cmus nextcloud-client
# albert
# vlc # remove it if parole is enough

# Enable Uncomplicated Firewall (UFW)
sudo ufw enable

# Remove unnecessary apps
sudo apt purge -y midori
sudo apt autoremove -y
