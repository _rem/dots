#!/usr/bin/env sh

# Update System
if [[ "$OSTYPE" == 'darwin'* ]]; then
  brew doctor && brew update && brew upgrade
  brew cleanup -n && brew prune -n
  # sudo softwareupdate --install --all
elif [[ "$OSTYPE" == 'linux-gnu' ]]; then
  . ~/.dots/update/apt_update.sh
fi

# Gem toolbox
. ~/.dots/update/system_gems.sh

# Prezto
cd ~/.zprezto
git pull upstream master && git submodule update --init --recursive
cd ~

# End update by running chkrootkit
rootkitchk && echo '' && echo "... update completed. Don't forget to review chkrootkit results!"

# Last remarks
if [[ "$OSTYPE" == 'darwin'* ]]; then
  echo "" && echo "Don't forget to brew cleanup && brew prune"
elif [[ "$OSTYPE" == 'linux-gnu' ]]; then
  # check for postgres and postgres-contrib upgrades
  apt list --upgradeable
fi
