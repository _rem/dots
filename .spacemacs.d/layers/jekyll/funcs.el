;;; funcs.el --- lunaryorn-markdown: Functions -*- lexical-binding: t; -*-

;;; Commentary:

;; Jekyll Utilities
;; New Post, Cheatsheet or Bit

;;; Code:

(defvar blog-root-dir "~/Projects/renemaya.github.io/")
(defvar drafts (concat blog-root-dir "_drafts"))
(defvar bits (concat blog-root-dir "_bits"))
(defvar posts (concat blog-root-dir "_posts"))

(defun today-is ()
  "Return current year-month-day."
  (format-time-string "%Y-%m-%d"))

(defun jekyll-new-post ()
  (let ((title
         (if (string= "" (thing-at-point 'word))
             (thing-at-point 'word) (car kill-ring))))
    (concat (file-name-as-directory drafts)
            (today-is) "-" title ".md")))

(defun jekyll-new-cheatsheet ()
  (let ((title
         (if (string= "" (thing-at-point 'word))
             (thing-at-point 'word) (car kill-ring))))
    (concat (file-name-as-directory drafts)
            (today-is) "-" title ".md")))

(defun jekyll-new-bit ()
  (let ((title
         (if (string= "" (thing-at-point 'word))
             (thing-at-point 'word) (car kill-ring))))
    (concat (file-name-as-directory bits)
            (today-is) "-" title ".md")))

;;; SPC aoc triggers org-capture
(setq org-capture-templates
      '(("b" "Bit" plain (file  (jekyll-new-bit))
         "---\ntitle: %? \ndate:\n---\n")
        ("p" "Post" plain (file  (jekyll-new-post))
         "---\nlayout: post\ntitle: %? \ndescription:\ncategories:\ntags: []\ndate:\n---\n")
        ("c" "Cheatsheet" plain (file  (jekyll-new-cheatsheet))
         "---\nlayout: cheatsheet\ntitle: %? \ndescription:\ncategories:\ndate:\n---\n")))

;; Drafts Management

(defun jekyll-drafts ()
  (let ((default-directory drafts))
    (file-expand-wildcards "*.md")))


(defun jekyll-resume (post)
  "Resume writing one of the posts from the Jekyll drafts directory"
  (interactive
   (list (completing-read "Post to resume: "
                          (jekyll-drafts) nil t "")))
  (find-file (concat (file-name-as-directory drafts) post)))


;; Publish drafts

(defun jekyll-publish (post)
  "Mark one of the posts from the Jekyll drafts directory as published.
    This actually means moving the post from the _drafts to the _posts
    directory."
  (interactive
   (list (completing-read "Post to mark as published: "
                          (jekyll-drafts) nil t "")))
  (copy-file (concat (file-name-as-directory drafts) post)
             (concat (file-name-as-directory posts) post))
  (delete-file (concat (file-name-as-directory drafts) post)))
