(defvar blorg-root-dir "~/Code/web/sites/ownware/")
(defvar blorg-posts (concat blorg-root-dir "code/drafts"))
(defvar blorg-articles (concat blorg-root-dir "configure/drafts"))

(defun blorg-new-post ()
  (let ((title
         (if (string= "" (thing-at-point 'word))
             (thing-at-point 'word) (car kill-ring))))
    (concat (file-name-as-directory blorg-posts)
            title ".org")))

(defun blorg-new-article ()
  (let ((title
         (if (string= "" (thing-at-point 'word))
             (thing-at-point 'word) (car kill-ring))))
    (concat (file-name-as-directory blorg-articles)
            title ".org")))


(setq org-capture-templates
      '(("d" "Code" plain (file (blorg-new-post))
         "#+Setupfile: ../templates/authors/rem.org
#+Date: {{{modification-time(%Y-%m-%d)}}}
#+Creator:
#+PubDate:
#+Title: ?
#+Keywords:
#+Description:

#+Begin_center
By: {{{author}}}
Published: {{{keyword(PubDate)}}}
Updated: {{{date}}}
#+End_center")
        ("g" "Configure" plain (file (blorg-new-article))
         "#+Setupfile: ../templates/authors/rem.org
#+Date: {{{modification-time(%Y-%m-%d)}}}
#+Creator:
#+PubDate:
#+Title: ?
#+Keywords:
#+Description:

#+Begin_center
By: {{{author}}}
Published: {{{keyword(PubDate)}}}
Updated: {{{date}}}
#+End_center")))

;; Drafts Management
;;
;; (defun jekyll-drafts ()
;;   (let ((default-directory drafts))
;;     (file-expand-wildcards "*.md")))


;; (defun jekyll-resume (post)
;;   "Resume writing one of the posts from the Jekyll drafts directory"
;;   (interactive
;;    (list (completing-read "Post to resume: "
;;                           (jekyll-drafts) nil t "")))
;;   (find-file (concat (file-name-as-directory drafts) post)))


;; Publish drafts
;;
;; (defun jekyll-publish (post)
;;   "Mark one of the posts from the Jekyll drafts directory as published.
;;     This actually means moving the post from the _drafts to the _posts
;;     directory."
;;   (interactive
;;    (list (completing-read "Post to mark as published: "
;;                           (jekyll-drafts) nil t "")))
;;   (copy-file (concat (file-name-as-directory drafts) post)
;;              (concat (file-name-as-directory posts) post))
;;   (delete-file (concat (file-name-as-directory drafts) post)))

;; check out some of these
;; https://out-of-cheese-error.netlify.com/spacemacs-config#org9b57a4f
