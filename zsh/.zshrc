# Set zle to vi mode
bindkey -v

# Remove PATH duplicate entries
# http://zsh.sourceforge.net/Doc/Release/Shell-Builtin-Commands.html#Shell-Builtin-Commands
typeset -U PATH path

# Zsh options docs: http://zsh.sourceforge.net/Doc/Release/Options.html
setopt EXTENDED_GLOB
source ~/.zprezto/init.zsh
unsetopt correct_all
unsetopt EXTENDED_HISTORY
source ~/.dots/zsh/.aliases

# Custom functions
for fun in ~/.dots/zsh/functions/*; do
  source $fun
done

# Env vars
export HISTSIZE=50
export SAVEHIST=50
export HISTCONTROL=ignoredups:ignorespace
export EDITOR="emacs"
# export EDITOR="spcemacs"
export VISUAL="nano"
export LESS="-EFgiMRswX -z-4"
export PAGER="less"
# export PAGER="nano --view"
# export PAGER="spcger"
export PATH="$PATH:`yarn global bin`"

if [[ "$OSTYPE" == "darwin"* ]]; then
  export HOMEBREW_NO_ANALYTICS=1
  export HOMEBREW_INSTALL_CLEANUP=1
  export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
  export LESSOPEN="| /usr/local/bin/src-hilite-lesspipe.sh %s"
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
  export PATH="$HOME/.local/bin:$PATH"
  export PATH="$PREFIX/lib/ry/current/bin:$PATH"
  # eval "$(ry setup)"
  # /usr/bin/keychain $HOME/.ssh/id_ed25519
  keychain id_ed25519
  source $HOME/.keychain/$HOSTNAME-sh
fi

# List ZSH config files for debugging purposes
# zsh -o SOURCE_TRACE
