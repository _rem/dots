cd_gem() {
  cd "$(bundle info --path $1)" 2> /dev/null

  if [ $? -ne 0 ]; then
    cd "$(dirname $(gem which $1))"
  fi
}
