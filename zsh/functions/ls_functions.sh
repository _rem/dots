ls_functions() {
  for func in ~/.dots/zsh/functions/*; do
    basename $func | sed 's/\.sh//'
  done
}
