validate_sha256() {
  if [ "$2" ]; then
    [ "$(shasum -a 256 $1)" = "$2" ] && (echo "valid")
  else
    echo "try: validate_sha256 path/to/download/as/in/integrity 'integrity sha256 line'"
  fi
}
