validate_md5() {
  if [ "$2" ]; then
    [ "$(md5 -q $1)" = "$2" ] && (echo "valid")
  else
    echo "try: validate_md5 /path/to/download integrity_md5_hash"
  fi
}

# $@ all the args
