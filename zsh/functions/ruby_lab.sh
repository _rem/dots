# Set of functions to create and delete a Ruby lab

# ruby_lab creates ~/Code/lab/ruby_lab and starts a REPL session
ruby_lab() {
  echo "\nBuilding lab...\n"
  git clone https://notabug.org/rem/ruby_lab.git ~/Code/lab/ruby_lab

  echo "\n\nEntering lab at:\n"
  cd ~/Code/lab/ruby_lab
  pwd

  echo "\n\nSetting tools up...\n"
  rm -rf .git
  bundle install -j2 --path .bundle/gems
  export PATH="$PWD/bin:$PATH"
  hash -r 2>/dev/null || true

  echo "\n\nType 'repl' to start a REPL session or upgrade the lab before that.\n\nHack on!"
}

# wipe_ruby_lab removes ~/Code/lab/ruby_lab
wipe_ruby_lab() {
    rm -rf ~/Code/lab/ruby_lab
    echo "...and like that, the ruby_lab is gone"
}
