fetch() {
  # this expects a file with a url per line
  while read line; do
    echo
    echo ${line}
    curl -O "${line}" --progress-bar
    sleep 2
  done < ${1}
  echo "DONE"
}
