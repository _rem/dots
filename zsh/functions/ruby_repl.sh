# Start repl
ruby_repl() {
    cd ~/CodingLabs/ruby_repl
    export PATH="$PWD/bin:$PATH"
    hash -r 2>/dev/null || true
    echo "REPL time!"
    repl
}
