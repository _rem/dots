# Create a new Ruby project based on my ruby_skeleton repo

# ruby_skeleton some_project creates ~/your/chosen/path/to/some_project
ruby_skeleton() {
  if [ "$1" ]; then
    echo "\nCloning Ruby skeleton to $1...\n"
    git clone git@github.com:renemaya/ruby_skeleton.git ~/"$1"
    cd ~/"$1"

    echo "\n\nInstalling dependencies in ./.bundle/gems ...\n"
    rm -rf .git .gitignore
    bundle install -j2 --path .bundle/gems
    export PATH="$PWD/bin:$PATH"
    hash -r 2>/dev/null || true

    echo "\n\nReady to run rake 'setup_project[YourProjectName]'"
  else
    echo "You need to set directory name ie. ruby_skeleton my_project"
  fi
}
