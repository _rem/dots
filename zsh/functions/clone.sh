# This function takes two arguments, both dir paths. It will clone the first dir
# to the second dir even if it doesn't exist. Cloning preserves the files attributes.

clone() {
    case "$1" in
        -h)
            echo "Use: clone ./source/path/. ./destination/path/"
            ;;
        *)
            if [ "$2" ]; then
                mkdir -p -- "$2"
                cp -a "$1" "$2"
            else
                echo "Expected two arguments. For help, clone -h."
            fi
            ;;
    esac
}
