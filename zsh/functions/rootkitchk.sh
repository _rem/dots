rootkitchk() {
  cd ~
  echo 'Start chkrootkit enter password to continue...'

  if [[ "$OSTYPE" == 'linux-gnu' ]]; then
    sudo chkrootkit -q -e -n
  elif [[ "$OSTYPE" == 'darwin'* ]]; then
    sudo chkrootkit -q -n
  fi

  cd -
}
