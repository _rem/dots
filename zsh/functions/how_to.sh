how_to() {
  # \ disables any associated alias temporarily
  \man "$@" 2> /dev/null

  # $? holds the exit code of the last command. 0 is OK, anything else an error
  # $? not equal to 0
  if [ $? -ne 0 ]; then
    "$1" -h
  fi
}
