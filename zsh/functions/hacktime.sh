# This function temporarily places the binstubs in local bin in front of $PATH
# Running 'hackTime my_project' also sets MY_PROJECT_ENV="development"

hackTime() {
  if [ "$1" ]; then
    name="$1_env"
    env="$(echo $name | tr '[:lower:]' '[:upper:]')"
    export "$env"="development"
    echo "$env set to 'development'"

    export PATH="$PWD/bin:$PATH"
    hash -r 2>/dev/null || true
    echo "binstubs added to path\n"

    echo "Hack on!"
  else
    echo "Project name needed ie. hackTime my_project"
  fi
}
