#!/bin/sh

ffx() {
  local fx

  fx="/Applications/FirefoxDeveloperEdition.app/Contents/MacOS/firefox-bin"

  case $1 in
    ^https) $fx --new-tab $1 ;;
    ^http)
      local url=$(echo $1 | sed -e "s/^http/https/")
      $fx --new-tab $url ;;
    *) $fx --new-tab "https://$1" ;;
  esac
}
