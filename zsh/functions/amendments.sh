# This function takes two arguments, both paths to files.
# Then prints their differences. Great for checking changes
# in user agreements/contracts.

amendments() {
  if [ "$2" ]; then
    wdiff "$1" "$2" | less
  else
    echo "Use: ${0##*/} /path/old_version.txt /path/new_version.txt"
    echo "  review amendments made to a document"
    exit 1
  fi
}
