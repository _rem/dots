#!/usr/bin/env sh
set -e

if [[ "$OSTYPE" == "darwin"* ]]; then
  . ~/.dots/install/brew.sh
  . ~/.dots/install/macOS_prefs.sh
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
  . ~/.dots/install/bodhi.sh
  . ~/.dots/install/bodhi_prefs.sh
fi

. ~/.dots/install/postgres.sh
. ~/.dots/install/symlinks.sh

# Spacemacs
git clone --recursive https://github.com/syl20bnr/spacemacs ~/.emacs.d
ln -sfv ~/.dots/.spacemacs.d ~/.spacemacs.d

# Prezto (Zsh)
git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

for rcfile in ~/.zprezto/runcoms/*; do
  rc=$(basename "$rcfile")

  if [[ "$rc" != "README.md" ]]; then
    ln -sfv "$rcfile" ~/.${rc}
  fi
done

ln -sfv ~/.dots/zsh/.zshrc ~/.zshrc
ln -sfv ~/.dots/zsh/.zpreztorc ~/.zpreztorc

# Gen ssh key
ssh-keygen -f ~/.ssh/id_ed25519 -t ed25519 -C "$USERNAME@$HOST"

# Finish up automated install
if [[ "$OSTYPE" == "darwin"* ]]; then
  echo '' && echo '' && echo 'Set Zsh as default shell. Restart terminal. Continue manually.'
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
  echo 'Enter password to set Zsh as default shell'
  chsh -s /usr/bin/zsh
  echo '' && echo '' && echo 'Log back in before continuing manually.'
fi
