* HardenedFDE

A Firefox Developer Edition ~user.js~ configuration file. HardenedFDE focuses on
privacy and security, not anonymity. Features relying on 3rd party services may
be partially on eg. ~safeBrowsing~.

** Considerations

Whilst this configuration might work on the Nightly distribution it probably
won't fully work on the current stable release, and certainly not on ESR. Beware,
since 58 ~toolkit.telemetry.enabled~ has been locked to ~true~ for pre-releases.
We can verify that uploads are disabled at ~about:telemetry~.

Settings in ~user.js~ take precedence over those in the preferences, and ~about:config~.
Any changes to those two will be lost on restart.

These are a few things to consider about this ~user.js~.

- Minimalist UI/UX settings.
- Disables geolocation.
- Spoofs basic regional settings for privacy due to tracking.
- Remove developer tools that can be abused.
- Disable WiFi hotspot checks.
- Cuts access to local network resources.
- Drops FTP handling.
- Forgets all new passwords.
- Disables form auto-fill.
- [Linux] Removes Gnome shell integration plugin.
- Disables all DRM content players.
- Disables screen sharing.
- Blocks camera, and mic use.

Some settings rely on external add-ons for better performance. More on those, and
other settings are explained in the Manual Settings section below.


** Requirements

- [[https://www.mozilla.org/en-US/firefox/developer/][Firefox Developer Edition]]


** Installation

- Download a copy of ~user.js~ file.
- Review settings.
- Set a single profile, or use system wide.
- Finalize with a few optional manual settings.


*** Review settings

Whether or not we review all setting, is good to have a view at the top two sections,
~VINTAGE~ and ~SENSITIVE~. They are meant for convenient access to options that
we may want to tune on a per-profile basis.


*** Single profile

As user.js changes the browser behavior, it is recommended to either:

- create a [[https://support.mozilla.org/en-US/kb/profile-manager-create-and-remove-firefox-profiles#w_manage-profiles-when-firefox-is-open][new profile]]
- [[https://support.mozilla.org/en-US/kb/back-and-restore-information-firefox-profiles][backup]] current profile

Note: when following instructions from those links you may need to replace the
directory paths with the ones below.

Copy ~user.js~ to desired user profile directory. The file should be located at:

- Linux =~/.mozilla/firefox/XXXXXXXX.your_profile_name/user.js=
- macOS =~/Library/Application Support/Firefox/Profiles/XXXXXXXX.your_profile_name=

*** system wide

Copy ~user.js~ to the Firefox installation directory. The file should be at:

- Linux: =/etc/firefox/firefox.js= or =/etc/firefox-esr/firefox-esr.js=
- macOS: =/Applications/FirefoxDeveloperEdition.app/Contents/Resources/mozilla.cfg=

In this file, we can substitute ~user_pref~ for:

- ~pref~ to set the default value for all profiles where it hasn't been set yet.
  Users can modify these via ~about:config~ page. Changes are kept across sessions.
- ~lockPref~ sets the default values for new profiles. Beware: these settings are
  locked and can't be changed neither through ~user.js~, nor the ~about:config~ page.

**** macOS

Create ~/Applications/FirefoxDeveloperEdition.app/Contents/Resources/defaults/pref/local-settings.js~

#+BEGIN_SRC js
pref("general.config.obscure_value", 0);
pref("general.config.filename", "mozilla.cfg");
#+END_SRC

If ~mozilla.cfg~ still fails to load, we must add a blank comment (~//~) at the
top of it.

*** Verify

Surf to ~about:support~, scroll down looking for "Important Modified Preferences"
and "user.js Preferences" sections.

Alternatively, go to ~about:config~ and check that ~_user.js.log~ is set to
~user.js loaded~

** Manual Settings

*** Passwords

Password storage has been disabled for security reasons. Make sure to install a
password manager that doesn't rely on the browser.

*** DNS over HTTPS

We can enable DoH starting w/Firefox 62. So it's been enabled in this ~user.js~
with default DoH resolver.

For other resolver options check out [[https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers][this list]] by the curl team, or even this
[[https://en.wikipedia.org/wiki/Public_recursive_name_server][Wikipedia page]].

*** Search engines

Whilst Firefox comes bundle with a few search engines we can add our preferred
ones.

Simply visit their main search page, and click the three dots next
the URL bar. Click on ~Add Search Engine~. Alternatively, purge unused engines.

We can add two [[https://stats.searx.xyz/][Searx instances]] to ensure we can always search the web privately.
Alternatively, you can use close-source solutions such as [[https://duckduckgo.com/][DuckDuckGo]], and [[https://www.startpage.com/][Startpage]].
Also, check out the official [[https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox][add or remove a search engine in firefox]] post.

*** Add-ons

Tracking is too profitable to be easy to turn off. These add-ons help a lot:

- [[https://addons.mozilla.org/firefox/addon/ublock-origin/][UBlock Origin]]
- [[https://addons.mozilla.org/en-US/firefox/addon/au-revoir-utm/][au-revoir utm]]
- [[https://addons.mozilla.org/firefox/addon/cookie-autodelete][Cookie Autodelete]]
- [[https://www.eff.org/https-everywhere][HTTPS Everywhere]]
- [[https://addons.mozilla.org/firefox/addon/decentraleyes/][Decentraleyes]]
- [[https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/][Multi-account containers]]
- [[https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/][Temporary containers]]
- [[https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/][Privacy Badger]]
- [[https://addons.mozilla.org/en-US/firefox/addon/crxviewer/][Extension source viewer]]

Read ~addons_config.org~ for sample configurations.

Some useful add-ons for web development (code not reviewed thoroughly):

- [[https://addons.mozilla.org/en-US/firefox/addon/laboratory-by-mozilla/][Laboratory]] - Content Security Policy toolkit
- [[https://addons.mozilla.org/en-US/firefox/addon/axe-devtools/][aXe]] - A11y developer tools
- [[https://addons.mozilla.org/en-US/firefox/addon/compat-report/][Compat report]] - Browser compatibility report
- [[https://addons.mozilla.org/en-US/firefox/addon/certainly-something/][Certainly Something]] - Certificate Viewer
- [[https://addons.mozilla.org/en-US/firefox/addon/darkreader/][Dark Reader]] - Dark theme for every website
- [[https://addons.mozilla.org/en-US/firefox/addon/a11y-outline/][a11y-outline]] - TOC-like page outline (toggle ~C-&~)

Other privacy-focus extensions we've seen recommended (may replace some of the above)

- [[https://addons.mozilla.org/en-US/firefox/addon/privacy-possum/][Privacy possum]]
- [[https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/][Skip redirect]]
- [[https://addons.mozilla.org/en-US/firefox/addon/canvasblocker/][CanvasBlocker]]
- [[https://addons.mozilla.org/en-US/firefox/addon/random_user_agent/][Random user agent]]. Source [[https://github.com/tarampampam/random-user-agent/][repo]], [[https://addons.mozilla.org/en-US/firefox/addon/random_user_agent/reviews/][acknowledged]].

Power-extensions that require more user interaction for a customized experienced.

- [[https://addons.mozilla.org/en-US/firefox/addon/noscript/][NoScript]]
- [[https://addons.mozilla.org/en-US/firefox/addon/umatrix/][uMatrix]]
- [[https://addons.mozilla.org/en-US/firefox/addon/requestcontrol/][Request control]]

Alternative viewers

- [[https://addons.mozilla.org/en-US/firefox/addon/qiureader/][QiuReader]] - ePub reader
- [[https://addons.mozilla.org/en-US/firefox/addon/epubreader/][EPUBReader]] - alternative ePub reader

*** Downloads

For privacy reasons, downloads aren't allowed on Desktop. To set your default
"downloads":

  General > Downloads > Save files to

*** Camera / Mic

Both, camera and mic, have been blocked for privacy reason. To add site
exceptions:

  Page Info > Permissions > Use the Camera/Microphone

To manage site exceptions:

  Options > Privacy & Security > Permissions > Camera/Microphone > Settings

*** Fonts

Web fonts can easily be abused on many ways, either directly or by
requiring unsafe configuration settings.

Best course of action, privacy wise, is to use whatever fonts Firefox
includes. Next, to set the ~font.name.*~ family of attributes to
something suitable to your system. If none of that is suitable check out
these:

- [[https://pagure.io/liberation-fonts][Liberation fonts]]
- [[https://www.typewolf.com/google-fonts][40 best google open-source fonts]]
- [[https://www.typewolf.com/blog/google-fonts-combinations][Pairings on google fonts]]

On macOS we might want to remove these fonts to reduce fingerprinting.
In ~Font Book~ remove ~Wingdings 2~, ~Wingdings 3~, ~Arial Unicode MS~,
~Brush Script MT~, ~Georgia~, and ~Helvetica~.

*** Notifications

Notifying is complex, requires features that may be abused. If you want to enable
them, in the ~VINTAGE~ section toggle:

- ~dom.serviceWorkers.enabled~
- ~dom.webnotifications.enabled~
- ~dom.webnotifications.serviceworker.enabled~

Even then notifications are blocked by default, yes they're that unsafe. To add
site exceptions:

    Page Info > Permissions > Receive Notifications

To manage site exceptions:

  Options > Privacy & Security > Permissions > Notifications > Settings

*** Temporary configuration

As mentioned above, we can temporarily reset any option on the ~about:config~
page. For instance, to toggle ~media.autoplay.enabled~ which may break some media
players. Also, to temporarily toggle on ~network.captive-portal-service.enabled~
to allow WiFi hotspot login pages. Overrides last until we restart the browser.

*** Updates

The ~user.js~, as is, has browser and add-on automatic updates turn on.
On BSD/Linux we may need to toggle ~app.update.auto~ under ~KEEP UP~.

** References

- [[http://kb.mozillazine.org/Profile_folder_-_Firefox][Firefox profile folder]]
- [[http://kb.mozillazine.org/User.js_file][user.js file]]
- [[http://kb.mozillazine.org/About:config_entries][about:config entries]]
- [[https://support.mozilla.org/en-US/kb/profile-manager-create-and-remove-firefox-profiles#w_manage-profiles-when-firefox-is-open][Create a profile with profile manager]]
- [[https://support.mozilla.org/en-US/kb/back-and-restore-information-firefox-profiles][Backup & restore profiles]]
- [[https://support.mozilla.org/en-US/kb/recovering-important-data-from-an-old-profile][Recover data from an old profile]]
- [[https://github.com/ghacksuserjs/ghacks-user.js][ghacks-user.js]]
- [[https://github.com/atomGit/Firefox-user.js/blob/master/user.js][ghacks-user.js supplement by 12bytes.org]]
- [[http://kb.mozillazine.org/Locking_preferences][Locking preferences]]
- [[https://github.com/DonQuixoteI/Firefox-UserGuide/blob/master/doc/locked-prefs.md][TL;DR Locking preferences]]
- [[https://wiki.archlinux.org/index.php/Firefox/Privacy#Remove_system-wide_hidden_extensions][Remove system-wide hidden extensions]]
- [[https://github.com/ghacksuserjs/ghacks-user.js/blob/cbea3adc7e4f9907737d40b94a0157ed3c2ed1b3/user.js#L360-L363][Silence of the Xpi]]
- [[http://12bytes.org/tech/firefox/firefox-search-engine-cautions-and-recommendations#adding-search-engines-to-firefox][Adding search engines to firefox]]
- [[https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox][Add or remove a search engine in firefox]]
- [[https://github.com/mozilla/pdf.js/wiki/Frequently-Asked-Questions][pdf.js faq]]
- [[https://web.archive.org/web/20180612075637/http://forums.mozillazine.org/viewtopic.php?p=14100263&sid=ccbb32d20108696ba1325be9ec741f4c][How to remove OpenH264 Video Codec]]
- [[https://web.archive.org/web/20180612075352/http://forums.mozillazine.org/viewtopic.php?f=38&t=2888681][How do I remove the OpenH264 Video Codec plugin??]]
- [[https://daniel.haxx.se/blog/2018/06/03/inside-firefoxs-doh-engine/][Inside Firefox’s DOH engine]]
- [[https://ffeathers.wordpress.com/2013/03/10/how-to-override-css-stylesheets-in-firefox/][How to override css stylesheets in Firefox]]
- [[https://www.hongkiat.com/blog/customize-reader-view-theme-firefox/][Customize reader view theme]]
