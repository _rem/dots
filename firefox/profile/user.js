/*
  user.js HardenedFDE
*/
// VINTAGE - personal UI/UX meddling
// SENSITIVE - personal sensitive data settings
// WAKE UP - window, and tab startup state.
// KEEP UP - updates
// MUTE PHONE HOME - anti-features
// QUIET - Security, tracking, and privacy settings
// SHARE - back-and-forth connection settings
// LISTEN - input handling
// FORGET - sensitive data handling
// CACHE - bloatware settings
// ISOLATE -
// REINFORCE
// sUX - intrusive UX/UI preferences
// RETIRED - soon to go on permanent vacation api
// Source: https://github.com/ghacksuserjs/ghacks-user.js/blob/master/user.js

/* Custom pref to check some syntax errors. Reaching the last one doesn't imply
 * success. Open the console right after startup for any warnings/error messages. */
user_pref("_user.js.log", "Start user.js integration");

// VINTAGE
user_pref("_user.js.log", "syntax error @ VINTAGE");
user_pref("devtools.theme", "dark");
user_pref("lightweightThemes.selectedThemeID", "firefox-compact-dark@mozilla.org"); // firefox-compact-light@mozilla.org, default-theme@mozilla.org
user_pref("general.autoScroll", false); // w/ middle-click [MAC]
user_pref("browser.download.forbid_open_with", true);
// Go brutalist on fonts. Prevent font fingerprinting
user_pref("layout.css.font-loading-api.enabled", false);
user_pref("browser.display.use_document_fonts", 0); // 0=false, 1=true. [linux]uBlock, breaks?
// Make sure either install these fonts, or customize according to what's available on your system
// user_pref("font.name.serif.x-unicode", "Times New Roman");
// user_pref("font.name.serif.x-western", "Times New Roman");
// user_pref("font.name.sans-serif.x-unicode", "Arial");
// user_pref("font.name.sans-serif.x-western", "Arial");
// user_pref("font.name.monospace.x-unicode", "Courier New");
// user_pref("font.name.monospace.x-western", "Courier New");
// user_pref("font.minimum-size.x-unicode", 12); // minimum font sizes
// user_pref("font.minimum-size.x-western", 12);
// user_pref("font.size.fixed.x-unicode", 16); // monospace font sizes
// user_pref("font.size.fixed.x-western", 16);
// user_pref("font.size.variable.x-unicode", 18); // default font sizes
// user_pref("font.size.variable.x-western", 18);
// user_pref("layers.acceleration.force-enable", true); // Affects fonts, and video performance
// or
// user_pref("layers.acceleration.disabled", true); // Affects fonts, and video performance
// user_pref("full-screen-api.enabled", false); // Disable screen resolution leak & video/game fullscreen controls
// user_pref("dom.event.contextmenu.enabled", false); // Breaks too many sites, but Shift-Right-Click always bring right-click context menu up
user_pref("media.autoplay.enabled", false); // tempo about:config if Breaks video click-to-play or control via context menu.
user_pref("devtools.chrome.enabled", false); // Disable Add-on devtools; disable remote debugging, limit scratchpad, defend self-XSS
user_pref("accessibility.typeaheadfind", true); // enable "Find As You Type"
user_pref("browser.backspace_action", 0); // 0=previous page, 1=scroll up, 2=do nothing
// user_pref("browser.ctrlTab.previews", true);
// user_pref("extensions.screenshots.disabled", true); // uncomment if no screenshots needed
user_pref("browser.tabs.closeWindowWithLastTab", true);
user_pref("browser.tabs.loadBookmarksInTabs", true); // open Bookmarks manager in a new tab
user_pref("browser.tabs.loadInBackground", true);
user_pref("browser.tabs.loadBookmarksInBackground ", false);
user_pref("browser.tabs.loadDivertedInBackground", true); // 3rd party programs open links in a new background tab
user_pref("browser.urlbar.decodeURLsOnCopy", true); // false if frequently visiting non-latin URL see  Bugzilla 1320061 (FF53+)
user_pref("ui.key.menuAccessKey", 0); // disable alt key toggling the menu bar [RESTART]
user_pref("identity.fxaccounts.enabled", false); // Disable and hide Firefox Accounts and Sync
user_pref("reader.parse-on-load.enabled", true); // "Reader View" [Android=false]
user_pref("browser.chrome.site_icons", false); // No favicon in tabs
user_pref("browser.chrome.favicons", false); // No favicons in  new bookmarks
user_pref("layout.css.visited_links_enabled", false); // Differentiate between visited and not visited links
user_pref("browser.triple_click_selects_paragraph", true);
user_pref("devtools.toolbox.zoomValue", "1.3"); // Developers Toolbox font size
user_pref("accessibility.tabfocus", 7); // Add to choose, 1=text fields only, 2=form elements NO text fields, 4=Links only
user_pref("general.smoothScroll", true); // enable/disable smooth scrolling
user_pref("general.smoothScroll.lines", true);// enable/disable smooth line scrolling (up/down arrow/page keys)
user_pref("general.smoothScroll.other", true); // enable/disable other smooth scrolling (Home/End keys)
user_pref("general.smoothScroll.pages", true);  // enable/disable page smooth scrolling (PgUp/PgDn keys)
user_pref("mousewheel.acceleration.factor", 10); // enabled if mousewheel.acceleration.start > -1
user_pref("mousewheel.acceleration.start", 0); // apply after N scroll clicks - must be > -1
user_pref("mousewheel.min_line_scroll_amount", 10);
// user_pref("permissions.default.shortcuts", 2); // Breaks the web. Block keyboard override. 0=1=allow, 2=block


// SENSITIVE
/*
  Addons coded w/latest API show a false positive for an older browser, as intended.
  To install them, temporarily toggle privacy.resistFingerprinting, and
  privacy.resistFingerprinting.block_mozAddonManager to false in about:config.
*/
user_pref("_user.js.log", "syntax error @ SENSITIVE");
user_pref("privacy.cpd.passwords", true); // Delete stored passwords
user_pref("dom.serviceWorkers.enabled", false); // Disable background tasks such as notifications
user_pref("dom.webnotifications.enabled", false);
user_pref("dom.webnotifications.serviceworker.enabled", false);
user_pref("places.history.enabled", true); // toggle off for shared computers.
user_pref("privacy.sanitize.timeSpan", 0); // History lapsed time to clear 0=all, 1=1hr, 2=2hrs, 3=4hrs, 4=today.
user_pref("network.captive-portal-service.enabled", false); // Disable checks for wi-fi hotspot
// Depending on both your local machine, and network's setups you may
// opt-in to use DoH. Ideally, use your own DoH server or that of someone you trust.
// If you don't mind using public servers check out
// https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers
user_pref("network.trr.uri", ""); // Searx for more options
user_pref("network.trr.custom_uri", ""); // match network.trr.uri
user_pref("network.trr.bootstrapAddress", ""); // only fill if available from trr.uri source
user_pref("network.trr.mode", 2); // set to 3 uses DoH only; may break some sites


// WAKE UP
user_pref("_user.js.log", "syntax error @ WAKE UP");
user_pref("browser.slowStartup.notificationDisabled", true); // Browser performance utility
user_pref("browser.slowStartup.maxSamples", 0);
user_pref("browser.slowStartup.samples", 0);
user_pref("browser.rights.3.shown", true); // #=> 'Know your rights' button already shown
user_pref("browser.startup.homepage_override.mstone", "ignore"); // Disable "What's new" page
user_pref("startup.homepage_welcome_url", "");
user_pref("startup.homepage_welcome_url.additional", "");
user_pref("startup.homepage_override_url", "");
user_pref("browser.feeds.showFirstRunUI", false);
user_pref("browser.shell.checkDefaultBrowser", true);
user_pref("browser.startup.homepage", "about:blank");
user_pref("browser.startup.page", 0); // 0=blank, 1=home, 2=last visited, 3=resume session
user_pref("browser.newtab.preload", false);
user_pref("browser.newtab.pinned", "");
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.enhanced", false);
user_pref("browser.newtabpage.directory.source", "data:text/plain,");
user_pref("browser.newtabpage.introShown", true);
user_pref("browser.newtabpage.activity-stream.enabled", false);
user_pref("browser.newtabpage.activity-stream.impressionId", "");
user_pref("browser.newtabpage.activity-stream.asrouterExperimentEnabled", false);
user_pref("browser.newtabpage.activity-stream.asrouter.providers.snippets", "");
user_pref("browser.newtabpage.activity-stream.default.sites", "");
user_pref("browser.newtabpage.activity-stream.disableSnippets", true);
user_pref("browser.newtabpage.activity-stream.asrouterfeed", false);
user_pref("browser.newtabpage.activity-stream.newtabinit", false);
user_pref("browser.newtabpage.activity-stream.places", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry.ping.endpoint", "");
user_pref("browser.newtabpage.activity-stream.telemetry.ut.events", false);
user_pref("browser.newtabpage.activity-stream.feeds.sections", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories.options", "data:text/plain,");
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.systemtick", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.topstories", false);
user_pref("browser.newtabpage.activity-stream.fxaccounts.endpoint", ""); // https://accounts.firefox.com
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.searchEngines", "");
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.havePinned", "");


// KEEP UP
user_pref("_user.js.log", "syntax error @ KEEP UP");
user_pref("app.update.enabled", true);
user_pref("app.update.auto", true); // BSD/Linux: false
user_pref("app.update.staging.enabled", false);
user_pref("app.update.silent", false);
user_pref("browser.search.update", false); // still, major engines seem to ignore it!
user_pref("browser.search.searchEnginesURL", "");
user_pref("extensions.update.enabled", true);
user_pref("extensions.update.autoUpdateDefault", true);
user_pref("extensions.systemAddon.update.enabled", false); // still get updated w/Firefox
// user_pref("extensions.systemAddon.update.url", "");
user_pref("lightweightThemes.update.enabled", false); // Disable personas updates
user_pref("services.blocklist.update_enabled", true);


// MUTE PHONE HOME
user_pref("_user.js.log", "syntax error @ MUTE PHONE HOME");
user_pref("dom.ipc.plugins.flash.subprocess.crashreporter.enabled", false);
user_pref("dom.ipc.plugins.reportCrashURL", false);
user_pref("extensions.getAddons.showPane", false); // Uses google analytics
user_pref("extensions.getAddons.cache.enabled", false); // Disable add-ons recommendations #UI
user_pref("extensions.webservice.discoverURL", ""); // "https://addons.mozilla.org/%LOCALE%/firefox/search?q=&platform=%OS%&appver=%VERSION%"
user_pref("devtools.onboarding.telemetry.logged", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false); // locked true in FDE and Nightly
user_pref("toolkit.telemetry.server", "data:,");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.cachedClientID", "");
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false); // Background Hang Reporter
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("breakpad.reportURL", ""); // crash reports url
user_pref("browser.tabs.crashReporting.sendReport", false); // #=> no report in about:tabcrashed
user_pref("browser.crashReports.unsubmittedCheck.enabled", false); // #=> no unsent report nagging
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false);
user_pref("browser.aboutHomeSnippets.updateUrl", "data:,"); // Disable homepage snippets from Mozilla
user_pref("browser.chrome.errorReporter.enabled", false);
user_pref("browser.chrome.errorReporter.submitUrl", "");
user_pref("browser.selfsupport.enabled", false); // no user rating telemetry
user_pref("browser.selfsupport.url", "");
user_pref("browser.ping-centre.telemetry", false);
user_pref("privacy.resistFingerprinting.block_mozAddonManager", true); // tied to extensions.webextensions.restrictedDomains
// user_pref("extensions.webextensions.restrictedDomains", ""); // Breaks mozilla sites
user_pref("network.allow-experiments", false);
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.api_url", "");
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("shield.savant.enabled", false);
user_pref("browser.library.activity-stream.enabled", false);
user_pref("permissions.manager.defaultsUrl", "");


// QUIET
user_pref("_user.js.log", "syntax error @ QUIET");
user_pref("geo.enabled", false);
user_pref("permissions.default.geo", 2); // 0=ask, 1=allow, 2=block
user_pref("geo.wifi.uri", ""); // "data:application/json,{}" or "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%"
user_pref("geo.wifi.logging.enabled", false);
user_pref("intl.locale.requested", "en-US"); // Spoof location
user_pref("intl.accept_languages", "en-US, en"); // Spoof accept_languages for improved privacy
user_pref("intl.regional_prefs.use_os_locales", false);
user_pref("browser.search.countryCode", "US"); // Spoof search engine country
user_pref("browser.search.region", "US"); // Spoof search engine region
user_pref("browser.search.geoip.url", "");
user_pref("browser.search.geoSpecificDefaults", false);
user_pref("browser.search.geoSpecificDefaults.url", "");// "https://search.services.mozilla.com/1/%APP%/%VERSION%/%CHANNEL%/%LOCALE%/%REGION%/%DISTRIBUTION%/%DISTRIBUTION_VERSION%"
user_pref("javascript.use_us_english_locale", true);
// user_pref("browser.privatebrowsing.autostart", true); // PB does not allow indexedDB breaks many Extensions
user_pref("browser.onboarding.enabled", false);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.pbmode.enabled", true);
user_pref("privacy.trackingprotection.ui.enabled", true);
user_pref("extensions.enabledScopes", 1);
user_pref("extensions.autoDisableScopes", 15);
user_pref("extensions.blocklist.enabled", true);
user_pref("extensions.blocklist.url", "https://blocklists.settings.services.mozilla.com/v1/blocklist/3/%APP_ID%/%APP_VERSION%/");
user_pref("xpinstall.whitelist.required", true);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
user_pref("browser.safebrowsing.blockedURIs.enabled", true);
user_pref("browser.safebrowsing.phishing.enabled", true);
user_pref("browser.safebrowsing.malware.enabled", true);
user_pref("browser.safebrowsing.downloads.remote.enabled", false); // remote application reputation lookup. Other protections intact
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.provider.google.reportURL", "");
user_pref("browser.safebrowsing.reportPhishURL", "");
user_pref("browser.safebrowsing.provider.google4.reportURL", "");
user_pref("browser.safebrowsing.provider.google.reportMalwareMistakeURL", "");
user_pref("browser.safebrowsing.provider.google.reportPhishMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.reportMalwareMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.reportPhishMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.dataSharing.enabled", false);
user_pref("browser.safebrowsing.provider.google4.dataSharingURL", "");
user_pref("devtools.remote.wifi.scan", false); // Disable android debugging DISABLE WEBIDE
user_pref("privacy.resistFingerprinting", true);
user_pref("device.sensors.enabled", false);
user_pref("dom.gamepad.enabled", false);
user_pref("dom.w3c_touch_events.enabled", 0); // leaks screen data 0=disabled, 1=enabled, 2=autodetect
user_pref("network.prefetch-next", false); // <a rel="next">
user_pref("network.dns.disablePrefetch", true);
user_pref("network.dns.disablePrefetchFromHTTPS", true);
user_pref("network.predictor.enabled", false);
user_pref("network.predictor.enable-prefetch", false);
user_pref("captivedetect.canonicalURL", "");
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("browser.send_pings", false); // <a ping="somewhere">
user_pref("browser.send_pings.require_same_host", true);
user_pref("security.ssl.disable_session_identifiers", true);
user_pref("security.ssl.errorReporting.automatic", false);
user_pref("security.ssl.errorReporting.enabled", false);
user_pref("security.ssl.errorReporting.url", "");
user_pref("security.tls.enable_0rtt_data", false); // TLS1.3 0-RTT (round-trip time); requires careful server architecture


// SHARE
user_pref("_user.js.log", "syntax error @ SHARE");
user_pref("network.dns.blockDotOnion", true);
user_pref("network.dns.disableIPv6", false);
user_pref("network.IDN_show_punycode", true);
user_pref("network.http.spdy.enabled", false);
user_pref("network.http.spdy.enabled.deps", false);
user_pref("network.http.spdy.enabled.http2", false);
user_pref("network.http.altsvc.enabled", false);
user_pref("network.http.altsvc.oe", false);
user_pref("network.proxy.socks_remote_dns", true); // #=> when available
user_pref("network.proxy.autoconfig_url.include_path", false);
user_pref("network.security.esni.enabled", true); //only if trr.mode >= 2
user_pref("security.csp.enable", true);
user_pref("security.csp.enable_violation_events", false);
user_pref("security.csp.experimentalEnabled", true);
user_pref("security.dialog_enable_delay", 700);
// Referer may need live tuning at about:config if stuff gets broken (rarely for legitimate stuff IMHO)
user_pref("network.http.sendRefererHeader", 1); // [checking] 2
user_pref("network.http.referer.trimmingPolicy", 2);
user_pref("network.http.referer.XOriginPolicy", 2); // [checking] 1
user_pref("network.http.referer.XOriginTrimmingPolicy", 2);
user_pref("network.http.referer.spoofSource", false);
user_pref("network.http.referer.defaultPolicy", 3);
user_pref("network.http.referer.defaultPolicy.pbmode", 2);
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.donottrackheader.value", 1);
user_pref("beacon.enabled", false); // Disable asynchronous HTTP transfers (abused by analytics)


// LISTEN
user_pref("_user.js.log", "syntax error @ LISTEN");
user_pref("network.file.disable_unc_paths", true); // Cut access to network resources.
user_pref("network.ftp.enabled", false); // Drop FTP, external ftp client better
user_pref("keyword.enabled", false); // Disable keywords to prevent leaking typos to search engine
user_pref("browser.fixup.alternate.enabled", false); // #=> no location bar domain guessing
user_pref("browser.urlbar.trimURLs", false);
user_pref("browser.sessionhistory.max_entries", 7); // per tab session history
user_pref("browser.urlbar.filter.javascript", true);
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.suggest.searches", false);
user_pref("browser.urlbar.userMadeSearchSuggestionsChoice", true); // stop prompting to toggle suggestions on
user_pref("browser.urlbar.usepreloadedtopurls.enabled", false);
user_pref("browser.urlbar.speculativeConnect.enabled", false);
user_pref("browser.urlbar.autocomplete.enabled", true); // must be true if any of history, bookmark, openpage is
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.suggest.bookmark", true);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.autoFill", true);
user_pref("browser.urlbar.oneOffSearches", false);
user_pref("browser.urlbar.maxHistoricalSearchSuggestions", 0);
user_pref("security.data_uri.block_toplevel_data_uri_navigations", true);


// FORGET
user_pref("_user.js.log", "syntax error @ FORGET");
user_pref("browser.formfill.enable", false);
user_pref("signon.rememberSignons", false); // Disable remember passwords. doesn't delete ones before setting
user_pref("signon.storeWhenAutocompleteOff", false); // to match signon.rememberSignons
user_pref("security.ask_for_password", 2); // 0=the first time (default), 1=every time it's needed, 2=every password_lifetime
user_pref("security.password_lifetime", 5); // in minutes
user_pref("signon.autofillForms", false); // Disable form autofill
user_pref("security.insecure_password.ui.enabled", true);
user_pref("browser.fixup.hide_user_pass", true);
user_pref("signon.formlessCapture.enabled", false);
user_pref("signon.autofillForms.http", false);
user_pref("security.insecure_field_warning.contextual.enabled", true);
user_pref("network.auth.subresource-img-cross-origin-http-auth-allow", false);
user_pref("network.cookie.cookieBehavior", 1);
// user_pref("dom.storage.enabled", false); // breaks a lot! rely on add-on for this
// user_pref("dom.indexedDB.enabled", false); // prevent tracking BUT uBlock et al use it.
user_pref("browser.formfill.expire_days", 0);
user_pref("browser.bookmarks.max_backups", 2);
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.clearOnShutdown.cache", true);
user_pref("privacy.cpd.cache", true);
user_pref("network.cookie.lifetimePolicy", 0); // assuming Cookie manager Add on; else 2
user_pref("privacy.clearOnShutdown.cookies", false); // assuming Cookie manager, else true
user_pref("privacy.cpd.cookies", true);
user_pref("privacy.clearOnShutdown.downloads", true);
user_pref("privacy.clearOnShutdown.formdata", true); // forms & search
user_pref("privacy.cpd.formdata", true);
user_pref("privacy.clearOnShutdown.offlineApps", true);
user_pref("privacy.cpd.offlineApps", true);
user_pref("privacy.clearOnShutdown.sessions", true); // active logins
user_pref("privacy.cpd.sessions", true);
user_pref("privacy.clearOnShutdown.siteSettings", false);
user_pref("privacy.cpd.siteSettings", true);
user_pref("privacy.clearOnShutdown.history", false);
user_pref("privacy.cpd.history", true);


// CACHE
user_pref("_user.js.log", "syntax error @ CACHE");
user_pref("dom.caches.enabled", false);
user_pref("browser.cache.offline.enable", false);
// user_pref("dom.storageManager.enabled", false); // only stats but if true cache.offline.enable must be true too!
user_pref("browser.cache.offline.insecure.enable", false);
user_pref("browser.cache.disk.enable", false); // assuming decentral eyes like add-on
user_pref("browser.cache.disk.capacity", 0);
user_pref("browser.cache.disk.smart_size.enabled", false);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.disk_cache_ssl", false);
user_pref("browser.cache.memory.enable", true);
user_pref("browser.cache.frecency_experiment", -1);
user_pref("browser.sessionstore.max_tabs_undo", 5);
user_pref("browser.sessionstore.max_windows_undo", 1); // enough for resume_from_crash to work
user_pref("browser.sessionstore.privacy_level", 2); // session restored metadata for  0=all. 1=unencrypted sites. 2=none.
user_pref("browser.sessionstore.resume_from_crash", true);
user_pref("browser.sessionstore.interval", 15000); // in milliseconds. Longer, more privacy but impairs 'Recently Closed Tabs'
user_pref("browser.shell.shortcutFavicons", false);
user_pref("extensions.webextensions.keepStorageOnUninstall", false);
user_pref("extensions.webextensions.keepUuidOnUninstall", false);


// ISOLATE
user_pref("_user.js.log", "syntax error @ ISOLATE");
user_pref("privacy.userContext.enabled", true); // Containers. Not available in Private Browsing
user_pref("privacy.userContext.ui.enabled", true);
user_pref("privacy.usercontext.about_newtab_segregation.enabled", true);
user_pref("privacy.userContext.longPressBehavior", 2);
user_pref("plugin.default.state", 0); // initial state; 0=disabled, 1=ask, 2=activate
user_pref("plugin.defaultXpi.state", 0);
user_pref("plugins.click_to_play", true);
user_pref("plugin.sessionPermissionNow.intervalInMinutes", 0);
user_pref("plugin.state.flash", 0);
user_pref("plugin.state.java", 0);
user_pref("plugin.state.libgnome-shell-browser-plugin", 0); // Gnome Shell Integration w/NPAPI plugin
user_pref("privacy.firstparty.isolate", true);
user_pref("privacy.firstparty.isolate.restrict_opener_access", true);
user_pref("xpinstall.signatures.required", true);


// REINFORCE
user_pref("_user.js.log", "syntax error @ REINFORCE");
// user_pref("security.ssl.require_safe_negotiation", true); // still breaks too many sites. rely on uBlock Origin
user_pref("security.tls.version.min", 3); // #=> TLS 1.2; [temp] 2 when SSL_ERROR_NO_CYPHER_OVERLAP
user_pref("security.tls.version.fallback-limit", 3);
user_pref("security.tls.version.max", 4); // #=> Enable TLS 1.3
user_pref("security.ssl.enable_ocsp_stapling", true); // verify w/o disclosing behavior to CA
user_pref("security.OCSP.enabled", 1); // 1 = true, 0 = false
user_pref("security.OCSP.require", true); // may break captive portals but disabled makes OCSP bypassable
user_pref("security.cert_pinning.enforcement_level", 2); // 0=disabled. 1=own MiTM (antivirus), 2=strict
user_pref("security.pki.sha1_enforcement_level", 1); // SHA-1 fam
user_pref("security.ssl3.rsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdhe_rsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_aes_128_sha", false);
user_pref("security.ssl3.dhe_rsa_aes_128_sha", false);
user_pref("security.ssl3.dhe_rsa_aes_256_sha", false);
// user_pref("security.ssl3.rsa_aes_128_sha", false); // still breaks too many sites
// user_pref("security.ssl3.rsa_aes_256_sha", false); // still breaks too many sites
user_pref("security.ssl3.ecdhe_ecdsa_rc4_128_sha", false); // RC4 fam
user_pref("security.ssl3.ecdhe_rsa_rc4_128_sha", false);
user_pref("security.ssl3.rsa_rc4_128_md5", false);
user_pref("security.ssl3.rsa_rc4_128_sha", false);


// sUX
user_pref("_user.js.log", "syntax error @ sUX");
user_pref("alerts.showFavicons", false); // #UI
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
user_pref("browser.ssl_override_behavior", 1);
user_pref("browser.xul.error_pages.expert_bad_cert", true);
user_pref("security.mixed_content.block_active_content", true);
user_pref("security.mixed_content.block_display_content", true); // may break images and other embedded stuff
user_pref("security.insecure_connection_icon.enabled", true); // #=> message shown when insecure site visited
user_pref("security.insecure_connection_text.enabled", true);
user_pref("security.insecure_connection_icon.pbmode.enabled", true);
user_pref("security.insecure_connection_text.pbmode.enabled", true);
user_pref("browser.download.folderList", 2);
user_pref("browser.download.useDownloadDir", false); // enforce user interaction for security
user_pref("browser.download.manager.addToRecentDocs", false);
user_pref("browser.download.hide_plugins_without_extensions", false);
user_pref("network.IDN_show_punycode", true);
user_pref("gfx.downloadable_fonts.enabled", true);
user_pref("gfx.font_rendering.opentype_svg.enabled", false);
user_pref("gfx.downloadable_fonts.woff2.enabled", false);
user_pref("font.blacklist.underline_offset", ""); // rare fonts which can be enumerated for fingerprinting.
user_pref("gfx.font_rendering.graphite.enabled", false);
user_pref("media.eme.enabled", false); // Disable DRM API for all plugins
user_pref("browser.eme.ui.enabled", false);
user_pref("media.gmp-provider.enabled", false);
user_pref("media.gmp.trial-create.enabled", false);
user_pref("media.gmp-manager.url", "data:text/plain,");
user_pref("media.gmp-manager.url.override", "data:text/plain,");
user_pref("media.gmp-manager.updateEnabled", false);
user_pref("media.gmp-widevinecdm.enabled", false); // Disable google's DRM video codec
user_pref("media.gmp-widevinecdm.visible", false);
user_pref("media.gmp-widevinecdm.autoupdate", false);
user_pref("media.gmp-gmpopenh264.enabled", false); // Disable bundled OpenH264 video codec
user_pref("media.gmp-gmpopenh264.autoupdate", false);
user_pref("media.peerconnection.enabled", false); // Disable WebRTC entirely
user_pref("media.peerconnection.use_document_iceservers", false);
user_pref("media.peerconnection.video.enabled", false);
user_pref("media.peerconnection.identity.enabled", false);
user_pref("media.peerconnection.identity.timeout", 1);
user_pref("media.peerconnection.turn.disable", true);
user_pref("media.peerconnection.ice.tcp", false);
user_pref("media.navigator.video.enabled", false); // WebRTC video capability
user_pref("media.peerconnection.ice.no_host", true);
user_pref("webgl.disabled", true);
user_pref("webgl.min_capability_mode", true);
user_pref("webgl.disable-extensions", true);
user_pref("webgl.disable-fail-if-major-performance-caveat", true);
user_pref("webgl.enable-webgl2", false);
user_pref("pdfjs.enableWebGL", false);
user_pref("media.getusermedia.screensharing.enabled", false); // Disable screensharing
user_pref("media.getusermedia.browser.enabled", false);
user_pref("media.getusermedia.audiocapture.enabled", false);
user_pref("permissions.default.camera", 2); // Block camera
user_pref("permissions.default.microphone", 2); // Block mic
user_pref("canvas.capturestream.enabled", false);
user_pref("dom.imagecapture.enabled", false); // Disable camera image capture
user_pref("gfx.offscreencanvas.enabled", false);
user_pref("media.block-autoplay-until-in-foreground", true);
user_pref("media.autoplay.allow-extension-background-pages", false);
user_pref("media.autoplay.allow-muted", false);
user_pref("media.autoplay.ask-permission", false);
user_pref("media.autoplay.block-event.enabled", true);
user_pref("media.autoplay.block-webaudio", true);
user_pref("media.autoplay.default", 1); // 0=Allowed, 1=Blocked, 2=Prompt
user_pref("dom.disable_window_open_feature.close", true);
user_pref("dom.disable_window_open_feature.location", true);
user_pref("dom.disable_window_open_feature.menubar", true);
user_pref("dom.disable_window_open_feature.minimizable", true);
user_pref("dom.disable_window_open_feature.personalbar", true); // aka bookmarks toolbar
user_pref("dom.disable_window_open_feature.resizable", true);
user_pref("dom.disable_window_open_feature.status", true);
user_pref("dom.disable_window_open_feature.titlebar", true);
user_pref("dom.disable_window_open_feature.toolbar", true);
user_pref("dom.disable_window_move_resize", true);
user_pref("browser.link.open_newwindow", 3); // Enforce on new tab
user_pref("browser.link.open_newwindow.restriction", 0);
user_pref("dom.disable_open_during_load", true);
user_pref("dom.popup_maximum", 3);
user_pref("dom.popup_allowed_events", "click dblclick"); // rest: "change mouseup notificationclick reset submit touchend"
user_pref("dom.push.enabled", false); // Disable push notifications
user_pref("dom.push.connection.enabled", false);
user_pref("dom.push.serverURL", "");
user_pref("dom.push.userAgentID", "");
user_pref("permissions.default.desktop-notification", 2); // Block notifications 0=always ask (default), 1=allow, 2=block
user_pref("dom.event.clipboardevents.enabled", false);
user_pref("dom.allow_cut_copy", false);
user_pref("dom.disable_beforeunload", true);
user_pref("dom.vibrator.enabled", false);
user_pref("javascript.options.asmjs", false);
user_pref("javascript.options.wasm", false); // [tempo enforced] disable WebAssembly unknown implications
user_pref("dom.IntersectionObserver.enabled", false);
user_pref("javascript.options.shared_memory", false);
user_pref("dom.vr.enabled", false);
user_pref("media.navigator.enabled", false); // Disable WebRTC getUserMedia
user_pref("dom.webaudio.enabled", false);
user_pref("dom.w3c_pointer_events.enabled", false);
user_pref("accessibility.force_disabled", 1);
user_pref("browser.helperApps.deleteTempFileOnExit", true);
user_pref("browser.pagethumbnails.capturing_disabled", true);
user_pref("browser.tabs.remote.allowLinkedWebInFileUriProcess", false); // may need temp reset in corporate environments
user_pref("browser.uitour.enabled", false);
user_pref("browser.uitour.url", "");
user_pref("devtools.webide.enabled", false);
user_pref("devtools.debugger.remote-enabled", false);
user_pref("devtools.webide.autoinstallADBHelper", false);
user_pref("devtools.webide.adbAddonURL", ""); // bk: https://ftp.mozilla.org/pub/mozilla.org/labs/fxos-simulator/adb-helper/#OS#/adbhelper-#OS#-latest.xpi
user_pref("devtools.webide.autoinstallFxdtAdapters", false);
user_pref("mathml.disabled", true); // Disable mathematical markup language
user_pref("middlemouse.contentLoadURL", false);
user_pref("network.http.redirection-limit", 10);
user_pref("webchannel.allowObject.urlWhitelist", "");
user_pref("ui.use_standins_for_native_colors", true); // may cause black on black for elements with undefined colors
// user_pref("pdfjs.disabled", false); // Not hard-coded just-in-case
// user_pref("pdfjs.disablePageMode", true); // Undocumented. NO idea what it does
user_pref("pdfjs.disableAutoFetch", true);
user_pref("pdfjs.disableFontFace", true); // safer but more resource-intensive
user_pref("pdfjs.disableStream", true); // Lazy page loading
user_pref("pdfjs.externalLinkTarget", 2); // 0 = default. 1 = replace window. 2 = new window/tab. 3 = parent. 4 = in top window.
user_pref("pdfjs.renderInteractiveForms", false);
user_pref("pdfjs.textLayerMode", 2); // 0 = Disabled. 1 = Enabled. 2 = Enabled w/enhanced selection.
user_pref("pdfjs.defaultZoomValue", "page-fit"); // 'auto', 'page-actual', 'page-width', 'page-height', 'page-fit', or a zoom level in percents.
user_pref("pdfjs.sidebarViewOnLoad", 0); // 0 = no sidebar. 1 = thumbnails. 2 = outline. 3 = attachments
// user_pref("svg.disabled", true); // Preferred, still breaks too many UI elements
user_pref("offline-apps.allow_by_default", false);
user_pref("network.manage-offline-status", false);
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.tabs.warnOnCloseOtherTabs", true);
user_pref("browser.tabs.warnOnOpen", true);
user_pref("browser.tabs.warnOnQuit", false);
user_pref("browser.showQuitWarning");
user_pref("full-screen-api.warning.delay", 0);
user_pref("full-screen-api.warning.timeout", 0);
user_pref("browser.download.autohideButton", false);
user_pref("browser.download.animateNotifications", false);
user_pref("toolkit.cosmeticAnimations.enabled", false);
user_pref("clipboard.autocopy", false); // Disable autocopy default [LINUX]
user_pref("layout.spellcheckDefault", 2); // 0=none, 1-multi-line, 2=multi-line & single-line
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.pocket.oAuthConsumerKey", "");
user_pref("extensions.screenshots.upload-disabled", true);
user_pref("extensions.formautofill.addresses.enabled", false);
user_pref("extensions.formautofill.available", "off");
user_pref("extensions.formautofill.creditCards.enabled", false);
user_pref("extensions.formautofill.heuristics.enabled", false);
user_pref("extensions.webcompat-reporter.enabled", false);
user_pref("image.animation_mode", "once"); // GIF animation repetitions
user_pref("lightweightThemes.usedThemes", "[]");
user_pref("lightweightThemes.getMoreURL", "");
user_pref("browser.discovery.enabled", false); // disable extension recommendations


// RETIRED
user_pref("_user.js.log", "syntax error @ RETIRED");
user_pref("experiments.enabled", false);
user_pref("experiments.manifest.uri", "");
user_pref("experiments.supported", false);
user_pref("experiments.activeExperiment", false);
user_pref("network.jar.block-remote-files", true);
user_pref("network.jar.open-unsafe-types", false);


// Proof-of-loaded. about:config should have this
user_pref("_user.js.log", "user.js loaded");
